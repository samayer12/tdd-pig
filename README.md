## TDD Exercise Information

Get an introduction to Test-Driven Development by coding along to [this example](http://www.codekoala.com/pdfs/tdd.pdf).
It's also hosted in this repository as [tdd.pdf](tdd.pdf).

Branch off of `main` to start your journey with `git checkout -b "<your_branch_name>"`.
The `.gitignore` file will keep you from committing irrelevant files, it was generated [here](https://www.toptal.com/developers/gitignore).

Use an IDE of your choice, I went with PyCharm.

Try to do it without blindly copy/pasting code from the PDF.
**If you get stuck**, checkout the diffs from the `example` branch.
Each commit corresponds to a code listing from the `tdd.pdf` guide.

Confused on where you are in the repo?
`git log --graph --oneline --decorate --all` is your friend.

## CI/CD Pipeline Information

A CI/CD pipeline is configured whenever you push to the remote server, check out what each job does in [.gitlab-ci.yml](.gitlab-ci.yml).
The script `local-pipeline.sh` runs the CI/CD pipeline locally.
With it, you can see if the pipeline should pass without pushing to the Gitlab server and waiting for it to run.

The following subsections are a brief overview of each stage in the pipeline.

### Build

Python isn't a compiled language, so our "build" step downloads dependencies for the [virtual environment](https://docs.python.org/3/tutorial/venv.html).

### Linters

This stage ensures your code meets quality standards.
We use [flake8](), [mypy](), and [pylint]() although other linters exist.
Feel free to modify your branch's CI/CD pipeline to use custom options or a different linter such as [black](https://github.com/psf/black).

### Tests

We run unit tests and calculate code coverage in this stage.

[coverage](https://coverage.readthedocs.io/) is used to calculate code coverage.
You can view the report in terminal output or as `html` from the job artifacts.

### Deploy

This stage [packages](https://packaging.python.org/tutorials/packaging-projects/) our project for distribution.
For the purposes of learning TDD, it's not important, but it demonstrates how CI/CD automatically produces software that meets quality checks.

It won't do anything, but you could download the `.whl` file from from the `wheel` job and install it with: `pip install <path to wheel>`.
Run `pip uninstall <path to wheel>` to remove it from your system.