from typing import Tuple, Any


class Pig:  # pylint: disable=too-few-public-methods

    def __init__(self, *players: Tuple[Any]) -> None:
        self.players = players

    def get_players(self) -> tuple[str, str, str]:
        """Return a tuple of all players"""
        self.players = ()
        return 'Player A', 'Player B', 'Player C'  # Placeholder for initial CI/CD run
