# Build
pip install -r requirements.txt
# Linters
flake8 src --max-line-length=120 --show-source --statistics
mypy ./src --strict
pylint ./src --rcfile=.pylintrc
# Test
pytest
coverage run -m unittest discover
coverage report -m
coverage html