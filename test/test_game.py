import unittest
from unittest import TestCase

from src.game import Pig


class GameTest(TestCase):

    def test_join(self):
        """Players may join a game of Pig"""
        expected = ('Player A', 'Player B', 'Player C')
        pig = Pig()
        result = pig.get_players()
        self.assertEqual(expected, result)  # Placeholder for initial CI/CD run


if __name__ == '__main__':
    unittest.main()
