import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pigtdd",  # Replace with your own username
    version="0.0.0",
    author="Example Author",
    author_email="author@example.com",
    description="A small example package that applies Test-Driven Development",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/samayer12/tdd-pig",
    project_urls={
        "Bug Tracker": "https://gitlab.com/samayer12/tdd-pig/-/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.6",
)
